class User < ActiveRecord::Base
  has_secure_password
  
  has_many :events, dependent: :destroy
  has_many :messages, dependent: :destroy
  has_many :joins, dependent: :destroy
  has_many :events_joined, through: :joins, source: :event
  # has_many :events_messaged, through: :messages, source: :event
  
  EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]+)\z/i
  validates :email, presence: true, uniqueness: { case_sensitive: false }, format: { with: EMAIL_REGEX }
  validates :first_name, :last_name, :location, :state, presence: true
  validates :password,length: { minimum: 8 }

  before_save { self.email.downcase! unless self.email.blank? }
end
# has_many :dog_breeds, :through => :dogs, :source => :breeds