class Event < ActiveRecord::Base
  belongs_to :user
  has_many :messages, dependent: :destroy
  has_many :joins, dependent: :destroy
  # has_many :users_messaged, through: :comments, source: :user
  has_many :users_joined, through: :joins, source: :user


  validates :event_name, :event_location, :event_state, presence: true
  validates :event_date, inclusion: { in: (Date.today..Date.today+5.years) }

  def event_date_format format = "%B %d, %Y"
    event_date.strftime(format)
 end
end
