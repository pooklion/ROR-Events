class UsersController < ApplicationController
  before_action :required_login, except: [:new, :create]
  before_action :required_correct_user, only: [:show, :edit, :update, :destroy]
  def new
    redirect_to root_path
  end

  def create
    user = User.new(first_name: params[:first_name], last_name: params[:last_name],email: params[:email], location: params[:location], state: params[:state],password: params[:password], password_confirmation: params[:password_confirmation])
    if user.valid?
      user.save
      flash[:messages] = ['Success!']
      redirect_to "/"
    else
      flash[:messages] = user.errors.full_messages
      redirect_to "/"
    end
    
  end
  def show
  end

  def edit
  end
end
