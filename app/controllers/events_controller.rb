class EventsController < ApplicationController
  def index
    @states = ['AL', 'AK', 'AS', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FM', 'FL', 'GA', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MH', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'MP', 'OH', 'OK', 'OR', 'PW', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'WY', 'AE', 'AA', 'AP']
    c = current_user.state
    @eventsIn = Event.joins(:user).where("event_state = ?", c).select("events.*, users.first_name")
    @events = Event.joins(:user).where.not("event_state = ?", c).select("events.*, users.first_name, users.last_name")

  end
  def create
    e = Event.new(event_name: params[:event_name], event_date: params[:event_date], event_location: params[:event_location], event_state: params[:event_state], user: current_user)
      if e.valid?
        e.save
        redirect_to "/events"
      else 
        flash[:messages] = e.errors.full_messages
        redirect_to "/events"
      end
  end

  def show
    @event = Event.find_by(id: params[:id])
    @a = @event.users_joined.all
    @messages = Message.joins(:user).where(event: @event)
  end

  def destroy
  end
end
