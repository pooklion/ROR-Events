class MessagesController < ApplicationController
  def create
    e = Event.find(params[:id])
    m = Message.new(event: e, user: current_user, content: params[:content])
    if m.valid?
      m.save
      redirect_to "/events/#{params[:id]}"
    else 
      redirect_to "/events/#{params[:id]}"
    end

  end

  def destroy
    e = Event.find(params[:id])
    join = e.joins.where(user: current_user)
    Join.delete(join)
    redirect_to "/events"
  end
end
