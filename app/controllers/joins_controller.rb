class JoinsController < ApplicationController
  def create
    e = Event.find(params[:id])
    join = Join.new(event: e, user: current_user)
    if join.valid?
      join.save
      redirect_to "/events"
    else 
      redirect_to "/events"
    end
  end

  def destroy
    e = Event.find(params[:id])
    join = e.joins.where(user: current_user)
    Join.delete(join)
    redirect_to "/events"
  end
end
