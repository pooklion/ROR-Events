Rails.application.routes.draw do
  post 'messages/create/:id' => 'messages#create'

  get 'messages/destroy/:id' => 'joins#destroy'

  get 'joins/create/:id' => 'joins#create'
  get 'joins/destroy/:id' => 'joins#destroy'

  get 'events/show'

  get 'events/destroy'
  get 'events' => 'events#index'
  post 'events' => 'events#create'
  get 'events/:id' => 'events#show'

  
  get 'users/:id' =>   'users#show'
  post 'users' =>   'users#create'

  post 'sessions' => 'sessions#create'
  get 'sessions/destroy' => 'sessions#destroy'
  root 'sessions#new'

end
